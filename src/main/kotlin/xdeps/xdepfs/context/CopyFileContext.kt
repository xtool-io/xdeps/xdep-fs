package xdeps.xdepfs.context


data class CopyFileContext(
    val path: String,
    val byteCountToDisplaySize: String = ""
)
