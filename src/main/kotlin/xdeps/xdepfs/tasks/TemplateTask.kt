package xdeps.xdepfs.tasks

import freemarker.cache.ClassTemplateLoader
import freemarker.template.Configuration
import freemarker.template.Template
import org.springframework.stereotype.Component
import xdeps.xdepfs.XdepFs
import java.io.StringReader
import java.io.StringWriter
import java.io.Writer

/**
 * Classe de task responsável por processar os arquivos de templates com o engine do FreeMarker.
 */
@Component
class TemplateTask {

    /**
     * Realiza a interpolação do arquivo de [template] proveniente do classpath definido em [classpathSource] com as variáveis [vars]
     * escrevendo a saída da interpolação em [writer].
     *
     * @param classpathSource String contendo o diretório base relativo ao classpath (src/main/resources).
     * @param template String contento o caminho relativo ao diretório base ([classpathSource]) do arquivo de template.
     * @param writer Objeto [Writer] onde será direcionado a interpolação do template com as variável [vars]
     * @param vars Mapa com as variáveis que serão interpoladas com o arquivo de template correspondente.
     */
    fun process(
        classpathSource: String,
        template: String,
        writer: Writer,
        vars: Map<String, Any>
    ) {
        val cfg = Configuration(Configuration.VERSION_2_3_31)
        val loader = ClassTemplateLoader(XdepFs::class.java.classLoader, "/${classpathSource.removePrefix("/")}")
        cfg.templateLoader = loader
        val tpl = cfg.getTemplate(template)
        tpl.process(vars, writer)
    }

    // FIXME Criar JavaDoc do método
    fun processInline(
        tpl: String,
        vars: Map<String, Any>
    ): String {
        val cfg = Configuration(Configuration.VERSION_2_3_29)
        cfg.defaultEncoding = "UTF-8";
        val stringWriter = StringWriter()
        val template = Template("", StringReader(tpl), cfg)
        template.process(vars, stringWriter)
        return stringWriter.toString()
    }

}
