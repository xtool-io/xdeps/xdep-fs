package xdeps.xdepfs.tasks

import org.apache.commons.io.FileUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import xdeps.xdepfs.context.CopyFileContext
import xdeps.xdepfs.utils.readResources
import java.nio.file.Files
import java.nio.file.Path

/**
 * Classe com tasks relacionadas a operações com sistema de arquivos.
 */
@Component
class FileSystemTask(private val templateTask: TemplateTask) {

    private val log: Logger = LoggerFactory.getLogger(FileSystemTask::class.java)

    /**
     * Realiza uma cópia com possibilidade de interpolação de arquivos usando o Freemarker.
     * Para habilitar a interpolação é necessário que o arquivo termina com a extensão *.ftl
     * Os arquivos iniciados com __ não serão copiados para o diretório de destino servido unicamente
     * para inclusão em outros arquivos de template.
     *
     * @param classpathSource String com o diretório do classpath a ser copiado
     * @param destination Path com o caminho onde serão copiados os arquivos.
     * @param vars Mapa com as variáveis que serão interpoladas com os arquivos de template.
     * @param overwrite Se true força a sobrescrita de arquivos caso já existam.
     * @param onEachCreated Lambda chamado para cada arquivo criado passando uma referência de CopyFileContext
     * @param onEachSkipped Lambda chamado para cada arquivo NÃO criado passando uma referência de CopyFileContext
     * @param only Se false a função não será executada.
     */
    fun copyFiles(
        classpathSource: String,
        destination: Path,
        vars: Map<String, Any>,
        overwrite: Boolean = false,
        onEachCreated: (ctx: CopyFileContext) -> Unit = {},
        onEachSkipped: (ctx: CopyFileContext) -> Unit = {},
        only: () -> Boolean = { true },
    ) {
        if (!only()) return
        // Carrega todos os arquivos do classpath com diretório base definido por [source]
        // ignorando os arquivos que iniciam com __
        val resources = readResources(classpathSource).filter { !it.filename?.startsWith("__")!! }
        for (resource in resources) {
            if (!resource.isReadable) continue
            // Remove toda a string anterior ao parâmetro source
            val template = resource.url
                .file
                .replace("%7b", "{") // Normaliza os caracteres encoded pela URL
                .replace("%7d", "}") // Normaliza os caracteres encoded pela URL
                .substringAfter(classpathSource)
                .removePrefix("/")
            log.debug("Template: {}", template)
            //
            val finalResourcePath = destination.resolve(
                templateTask.processInline(
                    template.removeSuffix(".ftl"),
                    vars
                )
            )
            if (Files.notExists(finalResourcePath)) {
                // Cria a hierarquia de diretório caso não exista
                if (Files.notExists(finalResourcePath.parent)) Files.createDirectories(finalResourcePath.parent)
                // Caso o arquivo seja um arquivo de template (FreeMarker)
                if (template.endsWith(".ftl")) {
                    val writer = Files.newBufferedWriter(finalResourcePath)
                    templateTask.process(
                        classpathSource,
                        template,
                        writer,
                        vars
                    )
                } else {
                    val os = Files.newOutputStream(finalResourcePath)
                    os.write(resource.inputStream.readBytes())
                    os.flush()
                    os.close()
                }
                onEachCreated(
                    CopyFileContext(
                        path = finalResourcePath.toString(),
                        FileUtils.byteCountToDisplaySize(resource.contentLength())
                    )
                )
                continue
            }
            onEachSkipped(
                CopyFileContext(
                    path = finalResourcePath.toString(),
                )
            )
        }
    }
}
