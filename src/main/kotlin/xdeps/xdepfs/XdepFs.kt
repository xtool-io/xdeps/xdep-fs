package xdeps.xdepfs

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan
class XdepFs

val log: Logger = LoggerFactory.getLogger(XdepFs::class.java)

