package xdeps.xdepcore

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.core.io.support.PathMatchingResourcePatternResolver
import xdeps.xdepfs.context.CopyFileContext
import xdeps.xdepfs.tasks.FileSystemTask
import xdeps.xdepfs.tasks.TemplateTask
import java.nio.file.Files
import java.nio.file.Paths

class FileSystemTaskTestCase {

    @Test
    fun read3ResourceFilesTest() {
        val templateTask = TemplateTask()
        val fsTask = FileSystemTask(templateTask)
        val source = "templates"
        val resources = PathMatchingResourcePatternResolver().getResources("classpath:${source}/**/*.*")
            .map { println(it.url.file.substringAfter(source).removePrefix("/")) }
        Assertions.assertEquals(7, resources.size)
    }

    @Test
    fun copyResourceFilesTest() {
        val templateTask = TemplateTask()
        val fsTask = FileSystemTask(templateTask)
        fsTask.copyFiles(
            classpathSource = "templates",
            destination = Paths.get("target/archetype"),
            vars = mapOf(
                "name" to "Fulano",
                "rootDir" to "br/jus/tre_pa"
            ),
            onEachCreated = { ctx: CopyFileContext -> println("${ctx.path} (${ctx.byteCountToDisplaySize})") }
        )
        Assertions.assertTrue(Files.exists(Paths.get("target/archetype/pom.xml")))
        Assertions.assertTrue(Files.exists(Paths.get("target/archetype/src/main/java/App.java")))
        Assertions.assertTrue(Files.exists(Paths.get("target/archetype/src/main/templates/Entity.java")))
    }
}
